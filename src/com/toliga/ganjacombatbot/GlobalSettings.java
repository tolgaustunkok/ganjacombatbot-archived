package com.toliga.ganjacombatbot;

import org.dreambot.api.methods.map.Tile;

public class GlobalSettings {
    public static String[] MOB_NAMES = null;
    public static boolean POWERKILL = false;
    public static boolean LOOT = false;
    public static boolean EAT_FOOD = false;
    public static boolean USE_POTION = false;
    public static boolean USE_SPECIAL_ATTACK = false;

    public static boolean BANK_WHEN_FULL = false;
    public static boolean LOGOUT_WHEN_FULL = false;
    public static boolean BURY_BONES = false;
    public static String[] LOOT_NAMES = null;

    public static Tile SOURCE_TILE = null;
}
